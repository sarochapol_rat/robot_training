*** Settings ***
Library           DateTime    #Suite Setup    initialize test    #Test Setup    Initialize TestData
Resource          ../../Keyword/login_keyword.txt
Resource          ../../Keyword/common_keywords.txt
Resource          ../../Keyword/irs_keyword.txt
Variables         ../../DataTests/${ENV}/irs/test_data_mobile_remittance.yaml
Variables         ../../Resources/common_config.yaml
Variables         ../../Resources/${ENV}/env_config.yaml
Suite Setup       Initialize test

*** Test Cases ***
TC_TMX_02564 Get transfer information with all mandatory parameters - Success
    ${reqID}    irs_keywords.Generate RequestID
    ${timestamp}    irs_keywords.Generate Timestamp
    ${resp}    irs_keywords.Get transfer info from barcode    ${irs_api_url}    ${mobile_gettransinfo_from_barcode}    ${access_token}    ${irs_barcode}    ${irs_terminal_id}
    ...    ${irs_branch_id}    ${irs_channel_id}    ${reqID}    ${timestamp}
    validate response    0    SUCCESS    ${resp}

TC_TMX_02565 Get transfer information with missing barcode_id - Fail
    ${reqID}    irs_keyword.Generate RequestID
    ${timestamp}    irs_keyword.Generate Timestamp
    ${resp}    irs_keyword.Get transfer info from barcode    ${irs_api_url}    ${mobile_gettransinfo_from_barcode}    ${access_token}    ${EMPTY}    ${irs_terminal_id}
    ...    ${irs_branch_id}    ${irs_channel_id}    ${reqID}    ${timestamp}
    validate response    4001    validation fail    ${resp}

TC_TMX_02566 Get transfer information with missing request_id - Fail
    ${reqID}    irs_keyword.Generate RequestID
    ${timestamp}    irs_keyword.Generate Timestamp
    ${resp}    irs_keyword.Get transfer info from barcode    ${irs_api_url}    ${mobile_gettransinfo_from_barcode}    ${access_token}    ${irs_barcode}    ${irs_terminal_id}
    ...    ${irs_branch_id}    ${irs_channel_id}    ${EMPTY}    ${timestamp}
    validate response    4001    validation fail    ${resp}

TC_TMX_02567 Get transfer information with missing terminal_id - Fail
    ${reqID}    irs_keyword.Generate RequestID
    ${timestamp}    irs_keyword.Generate Timestamp
    ${resp}    irs_keyword.Get transfer info from barcode    ${irs_api_url}    ${mobile_gettransinfo_from_barcode}    ${access_token}    ${irs_barcode}    ${EMPTY}
    ...    ${irs_branch_id}    ${irs_channel_id}    ${reqID}    ${timestamp}
    validate response    4001    validation fail    ${resp}

TC_TMX_02568 Get transfer information with missing branch_id - Fail
    ${reqID}    irs_keyword.Generate RequestID
    ${timestamp}    irs_keyword.Generate Timestamp
    ${resp}    irs_keyword.Get transfer info from barcode    ${irs_api_url}    ${mobile_gettransinfo_from_barcode}    ${access_token}    ${irs_barcode}    ${irs_terminal_id}
    ...    ${EMPTY}    ${irs_channel_id}    ${reqID}    ${timestamp}
    validate response    4001    validation fail    ${resp}

TC_TMX_02569 Get transfer information with missing channel_id - Fail
    ${reqID}    irs_keyword.Generate RequestID
    ${timestamp}    irs_keyword.Generate Timestamp
    ${resp}    irs_keyword.Get transfer info from barcode    ${irs_api_url}    ${mobile_gettransinfo_from_barcode}    ${access_token}    ${irs_barcode}    ${irs_terminal_id}
    ...    ${irs_branch_id}    ${EMPTY}    ${reqID}    ${timestamp}
    validate response    1001    Authentication fail    ${resp}

TC_TMX_02570 Get transfer information with missing timestamp - Fail
    ${reqID}    irs_keyword.Generate RequestID
    ${resp}    irs_keyword.Get transfer info from barcode    ${irs_api_url}    ${mobile_gettransinfo_from_barcode}    ${access_token}    ${irs_barcode}    ${irs_terminal_id}
    ...    ${irs_branch_id}    ${irs_channel_id}    ${reqID}    ${EMPTY}
    validate response    4001    validation fail    ${resp}

TC_TMX_02571 Get transfer information with incorrect barcode_id format - Fail
    ${reqID}    irs_keyword.Generate RequestID
    ${timestamp}    irs_keyword.Generate Timestamp
    ${resp}    irs_keyword.Get transfer info from barcode    ${irs_api_url}    ${mobile_gettransinfo_from_barcode}    ${access_token}    ${irs_barcode}99    ${irs_terminal_id}
    ...    ${irs_branch_id}    ${irs_channel_id}    ${reqID}    ${timestamp}
    validate response    4001    validation fail    ${resp}

TC_TMX_02572 Get transfer information with expired barcode_id format - Fail
    ${reqID}    irs_keyword.Generate RequestID
    ${timestamp}    irs_keyword.Generate Timestamp
    ${resp}    irs_keyword.Get transfer info from barcode    ${irs_api_url}    ${mobile_gettransinfo_from_barcode}    ${access_token}    278040000000002304    ${irs_terminal_id}
    ...    ${irs_branch_id}    ${irs_channel_id}    ${reqID}    ${timestamp}
    validate response    2103    Barcode 278040000000002304 is expired.    ${resp}

*** Keywords ***
Initialize test
    irs_keyword.TMX_IRS_mobile_user_login

Validate Response
    [Arguments]    ${expected_code}    ${expected_msg}    ${actual}
    ${RespJson}    evaluate    json.loads('''${actual.content}''')    json
    should be equal as integers    ${RespJson["response_code"]}    ${expected_code}
    should be equal as strings    ${RespJson["user_message"]}    ${expected_msg}
