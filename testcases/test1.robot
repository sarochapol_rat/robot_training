*** Settings ***
Documentation    Suite description
Library           RequestsLibrary
Library           Collections
Variables         ../resources/configs/common_config.yaml
Variables         ../resources/configs/${ENV}/env_config.yaml
Variables         ../resources/testdata/${ENV}/IRS/test_data_truemoney_transfer.yaml
Resource          ../keywords/api/irs_keywords.robot
Resource          ../keywords/api/login_keywords.robot


*** Test Cases ***
TEST1
    [Tags]    DEBUG
    Initialize test

*** Keywords ***
Initialize test
    irs_keywords.TMX_IRS_mobile_user_login