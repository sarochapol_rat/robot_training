*** Settings ***
Library           Collections
Library           RequestsLibrary
Library           OperatingSystem

*** Keywords ***
Send HTTP Post Request
    [Arguments]    ${URL}    ${path}    ${headers}    ${body}
    Create Session    acceptor    ${URL}    headers=${headers}
    ${resp}=    Post Request    acceptor    ${path}    ${body}
    Is HTTP Success    ${resp}
    [Return]    ${resp}

Send HTTP Get Request
    [Arguments]    ${URL}    ${path}    ${headers}
    Create Session    acceptor    ${URL}    headers=${headers}
    ${resp}=    Get Request    acceptor    ${path}
    Is HTTP Success    ${resp}
    [Return]    ${resp}

Is HTTP Success
    [Arguments]    ${resp}
    Should Be Equal As Integers    ${resp.status_code}    200    Error status=${resp.status_code}
