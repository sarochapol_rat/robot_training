*** Settings ***
Library           Collections
Library           RequestsLibrary
Library           DateTime


*** Keywords ***
TMX_IRS_mobile_get_member
    [Arguments]    ${MobileLoginURL}    ${GetMemberPath}    ${access_token}
    ${header}    Create Dictionary    irs-access-token=${access_token}    Authorization=Basic ZGV2OnRoaXMgaXMgYSBkZXY=
    Comment    ${body}    Set Variable    { "username": "${username}", "password":"${password}" }
    Comment    Create Session    getmember    ${MobileLoginURL}    headers=${header}
    Comment    ${response}    Get Request    getmember    ${GetMemberPath}
    Comment    Is HTTP Success    ${response}
    ${response}    common_keywords.Send_HTTP_Get_Request    ${MobileLoginURL}    ${GetMemberPath}    ${header}
    ${resp_json}=    To Json    ${response.content}
    [Return]    ${response}

TMX_IRS_mobile_list_sender
    [Arguments]    ${MobileLoginURL}    ${ListSenderPath}    ${access_token}
    ${header}    Create Dictionary    irs-access-token=${access_token}    Authorization=Basic ZGV2OnRoaXMgaXMgYSBkZXY=
    Comment    ${body}    Set Variable    { "username": "${username}", "password":"${password}" }
    Comment    Create Session    getmember    ${MobileLoginURL}    headers=${header}
    Comment    ${response}    Get Request    getmember    ${GetMemberPath}
    Comment    Is HTTP Success    ${response}
    ${response}    common_keywords.Send_HTTP_Get_Request    ${MobileLoginURL}    ${ListSenderPath}    ${header}
    ${resp_json}=    To Json    ${response.content}
    [Return]    ${response}

TMX_IRS_mobile_list_receiver
    [Arguments]    ${MobileLoginURL}    ${ListReceiverPath}    ${access_token}
    ${header}    Create Dictionary    irs-access-token=${access_token}    Authorization=Basic ZGV2OnRoaXMgaXMgYSBkZXY=
    Comment    ${body}    Set Variable    { "username": "${username}", "password":"${password}" }
    Comment    Create Session    getmember    ${MobileLoginURL}    headers=${header}
    Comment    ${response}    Get Request    getmember    ${GetMemberPath}
    Comment    Is HTTP Success    ${response}
    ${response}    common_keywords.Send_HTTP_Get_Request    ${MobileLoginURL}    ${ListReceiverPath}    ${header}
    ${resp_json}=    To Json    ${response.content}
    [Return]    ${response}

TMX_IRS_mobile_create_pretransfer
    [Arguments]    ${MobileLoginURL}    ${PreTransferPath}    ${access_token}   ${irs_member_id}    ${irs_sender_id}    ${irs_receiver_id}  ${purpose_of_transfer}
    ${header}    Create Dictionary    Content-Type=application/json     irs-access-token=${access_token}    Authorization=Basic ZGV2OnRoaXMgaXMgYSBkZXY=
    ${body}    Set Variable    { "member_id": "${irs_member_id}", "sender_id":"${irs_sender_id}", "receiver_id":"${irs_receiver_id}", "purpse_of_trasfer":"${purpose_of_transfer}"}
    Comment    Create Session    getmember    ${MobileLoginURL}    headers=${header}
    Comment    ${response}    Get Request    getmember    ${GetMemberPath}
    Comment    Is HTTP Success    ${response}
    ${response}    common_keywords.Send_HTTP_Post_Request    ${MobileLoginURL}    ${PreTransferPath}    ${header}   ${body}
    ${resp_json}=    To Json    ${response.content}
    [Return]    ${response}

TMX_IRS_mobile_get_exchange_rate
    [Arguments]     ${URL}    ${get_exchange_rate_path}    ${req_id}        ${orig_country_code}      ${orig_country_unit}
    ...     ${dest_country_code}    ${dest_country_unit}    ${transfer_amount}    #${content_signature}
    ${irs_timestamp}    irs_keyword.Generate Timestamp
    ${header}    Create Dictionary    secret=${irs_secret}    channel_id=${irs_channel_id}      #${content_signature}
    ${exchange_rate_resp}    common_keywords.Send HTTP Get Request    ${URL}      ${get_exchange_rate_path}?req_id=${req_id}&terminal_id=${irs_terminal_id}&branch_id=${irs_branch_id}&channel_id=${irs_channel_id}&original_country_code=${orig_country_code}&original_country_unit=${orig_country_unit}&destination_country_code=${dest_country_code}&destination_country_unit=${dest_country_unit}&timestamp=${irs_timestamp}&amount=${transfer_amount}    ${header}
    Is HTTP Success     ${exchange_rate_resp}
    [Return]    ${exchange_rate_resp}

Get transfer info from barcode
    [Arguments]    ${URL}    ${getTransferInfoPath}    ${access_token}    ${irs_barcode}    ${irs_terminal_id}    ${irs_branch_id}
    ...    ${irs_channel_id}    ${irs_req_id}    ${irs_timestamp}
    ${header}    Create Dictionary    secret=${irs_secret}    channel_id=${irs_channel_id}
    ${transferInfoResp}    common_keywords.Send HTTP Get Request    ${URL}    ${getTransferInfoPath}/${irs_barcode}?req_id=${irs_req_id}&terminal_id=${irs_terminal_id}&branch_id=${irs_branch_id}&channel_id=${irs_channel_id}&timestamp=${irs_timestamp}    ${header}
    [Return]    ${transferInfoResp}

TMX_IRS_mobile_user_login
    # Mobile user login
    ${access_token}    ${irs_member_id}    login_keywords.TMX_IRS_mobile_login    ${irs_api_url}    ${mobile_login_path}    ${mobile_user_1.username}    ${mobile_user_1.password}
    set suite variable    ${access_token}    ${access_token}
    # Get Member information
    ${irs_member}    irs_keywords.TMX_IRS_mobile_get_member    ${irs_api_url}    ${mobile_getmember_path}    ${access_token}
    ${json}    To Json    ${irs_member.content}
    should be equal as integers    ${json["response_code"]}    0    ERROR:    Cannot get member info
    set suite variable    ${irs_member_id}    ${json["id"]}
    #log    ${irs_member_id}
    # List sender information
    ${senderList}    irs_keywords.TMX_IRS_mobile_list_sender    ${irs_api_url}    ${mobile_listsender_path}    ${access_token}
    #${senderListJson}    To Json    ${senderList.content}
    ${senderListjson}    evaluate    json.loads('''${senderList.content}''')    json
    ${senderListSize}    get length    ${senderListjson}
    should not be equal as integers    ${senderListSize}    0    ERROR: No sender information found
    should not be empty    ${senderListjson[0]["id"]}
    log    ${senderListjson[0]["id"]}
    set suite variable    ${irs_sender_id}    ${senderListjson[0]["id"]}
    # List receiver information
    ${receiverList}    irs_keywords.TMX_IRS_mobile_list_receiver    ${irs_api_url}    ${mobile_listreceiver_path}    ${access_token}
    ${receiverListjson}    evaluate    json.loads('''${receiverList.content}''')    json
    ${receiverListSize}    get length    ${receiverListjson}
    should not be equal as integers    ${receiverListSize}    0    ERROR: No receiver information found
    should not be empty    ${receiverListjson[0]["id"]}
    set suite variable    ${irs_receiver_id}    ${receiverListjson[0]["id"]}
    # Create pre-transfer & generate barcode
    ${preTransfer}    irs_keywords.TMX_IRS_mobile_create_pretransfer    ${irs_api_url}    ${mobile_pretransfer_path}    ${access_token}    ${irs_member_id}    ${irs_sender_id}
    ...    ${irs_receiver_id}    robottest
    ${preTransferJson}    evaluate    json.loads('''${preTransfer.content}''')    json
    ${preTransferSize}    get length    ${preTransferJson}
    should not be equal as integers    ${preTransferSize}    0    ERROR: No barcode information found
    should be equal as integers    ${preTransferJson["response_code"]}    0    ERROR:    Cannot get Pre-transfer info
    set suite variable    ${irs_pretransfer_id}    ${preTransferJson["id"]}
    set suite variable    ${irs_barcode}    ${preTransferJson["barcode"]}
    set suite variable    ${irs_sender_identifier_no}    ${preTransferJson["sender"]["identifier_no"]}

Generate RequestID
    ${irs_req_id}    get time    epoch
    [Return]    ${irs_req_id}

Generate Timestamp
    ${irs_timestamp}    DateTime.get current date    result_format=%Y-%m-%dT%H:%M:%S
    ${irs_timestamp}    set variable    ${irs_timestamp}%2B07:00
    [Return]    ${irs_timestamp}

TMX_IRS_mobile_charge_confirm
    [Arguments]    ${URL}    ${Path}    ${barcode}  ${channelID_header}   ${channelID_body}   ${secret}   ${reqID}  ${terminalID}    ${branchID}
    ...     ${agentID}      ${sender_exchange_rate}     ${receiver_exchange_rate}    ${fee}    ${amount}
    ...     ${licenseID}    ${timestamp}
    ${header}    Create Dictionary    Content-Type=application/json     channel_id=${channelID_header}    secret=${secret}
    ${body}    Set Variable    { "req_id": "${reqID}", "terminal_id":"${terminalID}","branch_id":"${branchID}","channel_id":"${channelID_body}","agent_id":"${agentID}","sender_exchange_rate":"${sender_exchange_rate}","receiver_exchange_rate":"${receiver_exchange_rate}","fee":"${fee}","amount":"${amount}","timestamp":"${timestamp}","license_id":"${licenseID}"}
    ${response}    common_keywords.Send_HTTP_Post_Request    ${URL}    ${Path}/${barcode}    ${header}   ${body}
    [Return]    ${response}

TMX_IRS_mobile_send_SMS
    [Arguments]    ${URL}    ${Path}    ${trans_id}  ${channelID_header}   ${channelID_body}   ${secret}   ${reqID}  ${terminalID}    ${branchID}
    ...     ${agentID}
    #${reqID}    irs_keyword.Generate RequestID
    #${timestamp}    Generate Timestamp
    #${timestamp}    set variable  2017-10-03T14:37:26+07:00
    ${header}    Create Dictionary    Content-Type=application/json     channel_id=${channelID_header}    secret=${secret}
    ${body}    Set Variable    { "req_id": "${reqID}", "terminal_id":"${terminalID}","branch_id":"${branchID}","channel_id":"${channelID_body}","agent_id":"${agentID}"}
    ${response}    common_keywords.Send_HTTP_Post_Request    ${URL}    ${Path}/${trans_id}    ${header}   ${body}
    ${resp_json}=    To Json    ${response.content}
    [Return]    ${response}
