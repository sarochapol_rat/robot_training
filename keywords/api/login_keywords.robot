*** Settings ***
Library           RequestsLibrary
Library           Collections
#Variables         ../Resources/common_config.yaml
#Variables         ../resources/config/${ENV}/env_config.yaml
Resource          ../common/common_keywords.robot

*** Keywords ***
TMX_IRS_mobile_login
    [Arguments]    ${MobileLoginURL}    ${MobileLoginPath}    ${username}    ${password}
    [Documentation]    True Transfer at 7-11 project
    ...
    ...
    ...    This keyword is for True Transfer @ 7-11 to login through TMX-API-IRS from mobile app.
    ${header}    Create Dictionary    Content-Type=application/json    Authorization=Basic ZGV2OnRoaXMgaXMgYSBkZXY=
    ${body}    Set Variable    { "username": "${username}", "password":"${password}" }
    ${response}    common_keywords.Send_HTTP_Post_Request    ${MobileLoginURL}    ${MobileLoginPath}    ${header}    ${body}
    ${resp_json}=    To Json    ${response.content}
    ${access_token}    Set Variable    ${resp_json["access_token"]}
    ${irs_member_id}    Set Variable    ${resp_json["id"]}
    [Return]    ${access_token}    ${irs_member_id}